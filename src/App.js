import React from 'react';
import './_vars.scss';
import './App.scss';
import Todo from './components/Todo/Todo';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Completed from './components/Completed/Completed';
import { Container, Row, Col } from 'react-bootstrap';
import Header from './components/Header/Header';
import ThemeContext from './contexts/ThemeContext';
import ThemeSwitch from './components/UI/ThemeSwitch/ThemeSwitch';

class App extends React.Component {
  constructor() {
    super();

    let items = JSON.parse(localStorage.getItem('items'));

    this.state = {
      items: items ? items : {},
      theme: 'light',
    };
  }

  saveItem(item = {}) {
    let items = { ...this.state.items };
    console.log(item);
    if (!item.id) {
      let latestIndex = localStorage.getItem('latestIndex');
      latestIndex = latestIndex ? latestIndex : 0;

      items[++latestIndex] = {
        id: latestIndex,
        title: item.title,
        text: item.text,
        completed: 0,
      };

      localStorage.setItem('latestIndex', latestIndex);
    } else {
      items[item.id].title = item.title;
      items[item.id].text = item.text;
    }

    localStorage.setItem('items', JSON.stringify(items));
    this.setState({ items: items });
  }

  onChangeCompleted = (id, isCompleted) => {
    let items = { ...this.state.items };

    items[id].completed = isCompleted;

    localStorage.setItem('items', JSON.stringify(items));
    this.setState({ items: items });
  };

  changeTheme = (theme) => {
    this.setState({ theme: theme });
  };

  render() {
    return (
      <ThemeContext.Provider value={this.state.theme}>
        <div className={`${this.state.theme}-theme main-wrapper`}>
          <ThemeSwitch changeTheme={this.changeTheme} />
          <Router>
            <Container>
              <Row>
                <Col>
                  <Header />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Switch>
                    <Route path="/" exact>
                      <Todo
                        saveItem={(item) => {
                          this.saveItem(item);
                        }}
                        items={this.state.items}
                        onChangeCompleted={this.onChangeCompleted}
                      />
                    </Route>
                    <Route path="/completed" exact>
                      <Completed
                        items={this.state.items}
                        onChangeCompleted={this.onChangeCompleted}
                      />
                    </Route>
                  </Switch>
                </Col>
              </Row>
            </Container>
          </Router>
        </div>
      </ThemeContext.Provider>
    );
  }
}

export default App;
