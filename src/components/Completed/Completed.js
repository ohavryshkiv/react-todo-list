import React from 'react';
import Items from '../Items/Items';

class Completed extends React.Component {
  render() {
    return (
      <div>
        <Items
          items={this.props.items}
          onChangeCompleted={this.props.onChangeCompleted}
          completed={1}
        />
      </div>
    );
  }
}

export default Completed;
