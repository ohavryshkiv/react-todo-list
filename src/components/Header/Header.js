import React from 'react';
import './Header.scss';
import { NavLink } from 'react-router-dom';

class Header extends React.Component {
  render() {
    return (
      <header>
        <ul className="main-nav">
          <li>
            <NavLink to="/" activeClassName="active" exact>
              Todo
            </NavLink>
          </li>
          <li>
            <NavLink to="/completed" activeClassName="active" exact>
              Completed
            </NavLink>
          </li>
        </ul>
      </header>
    );
  }
}

export default Header;
