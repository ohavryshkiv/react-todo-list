import React from 'react';
import Items from '../Items/Items';
import Manage from '../Items/Manage/Manage';
import DefaultButton from '../UI/DefaultButton/DefaultButton';

class Todo extends React.Component {
  constructor() {
    super();

    this.state = {
      openedCreatePopup: false,
    };
  }

  handleShowCreatePopup() {
    this.setState({ openedCreatePopup: true });
  }

  handleHideCreatePopup() {
    this.setState({ openedCreatePopup: false });
  }

  render() {
    return (
      <div>
        <Manage
          show={this.state.openedCreatePopup}
          onHide={() => {
            this.handleHideCreatePopup();
          }}
          saveItem={this.props.saveItem}
        />

        <DefaultButton
          onClick={() => {
            this.handleShowCreatePopup();
          }}
        >
          Create
        </DefaultButton>

        <Items
          items={this.props.items}
          onChangeCompleted={this.props.onChangeCompleted}
          completed={0}
          saveItem={this.props.saveItem}
        />
      </div>
    );
  }
}

export default Todo;
