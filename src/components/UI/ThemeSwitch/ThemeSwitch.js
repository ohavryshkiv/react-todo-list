import React from 'react';
import './ThemeSwitch.scss';

class ThemeSwitch extends React.Component {
  handleChange = (event) => {
    this.props.changeTheme(event.target.checked ? 'dark' : 'light');
  };

  render() {
    return (
      <div className="theme-switch-wrapper">
        <label className="theme-switch" htmlFor="theme-switch">
          <input type="checkbox" id="theme-switch" onChange={(event) => this.handleChange(event)} />
          <div className="slider round" />
        </label>
      </div>
    );
  }
}

export default ThemeSwitch;
