import React from 'react';
import './DefaultButton.scss';
import ThemeContext from '../../../contexts/ThemeContext';

class DefaultButton extends React.Component {
  render() {
    return (
      <ThemeContext.Consumer>
        {(context) => (
          <button className={`default-site-btn ${context}`} onClick={this.props.onClick}>
            {this.props.children}
          </button>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default DefaultButton;
