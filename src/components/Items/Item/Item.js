import React from 'react';
import './Item.scss';
import { Row, Col } from 'react-bootstrap';
import ThemeContext from '../../../contexts/ThemeContext';

class Item extends React.Component {
  render() {
    let itemClassNames = ['todo-item'];
    if (this.props.params.completed) {
      itemClassNames.push('completed');
    }

    return (
      <ThemeContext.Consumer>
        {(context) => (
          <div className={itemClassNames.join(' ') + ' ' + context}>
            <Row>
              <Col xs="1">
                <button
                  className="todo-check"
                  onClick={() => {
                    this.props.onChangeCompleted(
                      this.props.params.id,
                      this.props.params.completed ? 0 : 1,
                    );
                  }}
                />
              </Col>
              <Col
                xs="11"
                onClick={() => {
                  this.props.handleShowPopup(this.props.params);
                }}
              >
                {this.props.params.title}
              </Col>
            </Row>
          </div>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default Item;
