import React from 'react';
import Item from './Item/Item';
import Manage from './Manage/Manage';

class Items extends React.Component {
  constructor() {
    super();

    this.state = {
      openedPopup: false,
      itemToEdit: {},
    };
  }

  handleShowPopup(item) {
    this.setState({
      openedPopup: true,
      itemToEdit: item,
    });
  }

  handleHidePopup() {
    this.setState({ openedPopup: false });
  }

  getItemList = () => {
    const items = this.props.items;
    return Object.keys(items).map((key) => {
      if (this.props.completed === items[key].completed) {
        return (
          <Item
            key={items[key].id}
            params={items[key]}
            onChangeCompleted={this.props.onChangeCompleted}
            handleShowPopup={(item) => {
              this.handleShowPopup(item);
            }}
          />
        );
      }
      return null;
    });
  };

  render() {
    return (
      <>
        <Manage
          show={this.state.openedPopup}
          onHide={() => {
            this.handleHidePopup();
          }}
          saveItem={this.props.saveItem}
          itemToEdit={this.state.itemToEdit}
        />
        {this.getItemList()}
      </>
    );
  }
}

export default Items;
