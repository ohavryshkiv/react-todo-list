import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

class Manage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: {
        title: props.itemToEdit ? props.itemToEdit.title : '',
        text: props.itemToEdit ? props.itemToEdit.text : '',
      },
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        fields: {
          title: this.props.itemToEdit ? this.props.itemToEdit.title : '',
          text: this.props.itemToEdit ? this.props.itemToEdit.text : '',
        },
      });
    }
  }

  handleChangeInput(event) {
    let fields = { ...this.state.fields };
    fields[event.target.name] = event.target.value;
    this.setState({ fields });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.saveItem({
      id: this.props.itemToEdit ? this.props.itemToEdit.id : null,
      title: this.state.fields.title,
      text: this.state.fields.text,
    });

    this.hidePopup();
  }

  hidePopup() {
    this.setState({
      fields: {
        title: '',
        text: '',
      },
    });

    this.props.onHide();
  }

  render() {
    let title = this.props.itemToEdit ? 'Edit TODO item' : 'Create TODO item';

    return (
      <Modal
        show={this.props.show}
        onHide={() => {
          this.hidePopup();
        }}
      >
        <form
          onSubmit={(event) => {
            this.handleSubmit(event);
          }}
        >
          <Modal.Header closeButton>
            <Modal.Title>{title}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                name="title"
                value={this.state.fields.title}
                onChange={(event) => {
                  this.handleChangeInput(event);
                }}
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlInput1">
              <Form.Label>Text</Form.Label>
              <Form.Control
                type="text"
                as="textarea"
                name="text"
                value={this.state.fields.text}
                onChange={(event) => {
                  this.handleChangeInput(event);
                }}
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => {
                this.hidePopup();
              }}
            >
              Close
            </Button>
            <Button variant="primary" type="submit">
              Save
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }
}

export default Manage;
